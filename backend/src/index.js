const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const chalk = require('chalk');
const http = require('http');
const routes = require('./routes');
const { setupWebsocket } = require('./websocket');



const app = express();

const server = http.Server(app);

setupWebsocket(server);

mongoose.connect('mongodb+srv://omnistack:omnistack@cluster0-dgkje.mongodb.net/semana10?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log(chalk.green("[mongoose] Connected to Database"));
}).catch((err) => {
    console.log(chalk.red("Not Connected to Database ERROR! "), chalk.red(err));
});

app.use(cors());
app.use(express.json());
app.use(routes);


server.listen(3333);