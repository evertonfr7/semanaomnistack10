const { Router } = require('express');
const DevController = require('./controllers/DevController');
const SearchController = require('./controllers/SearchController');

const routes = Router();

routes.get('/', (req, res) => {
    return res.json();
});

routes.get('/devs', DevController.index);
routes.get('/search', SearchController.index);

routes.post('/devs', DevController.store);
routes.get('/dev', DevController.destroy);
routes.get('/update', DevController.update);


module.exports = routes;