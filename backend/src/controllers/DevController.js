const axios = require('axios');
const Dev = require('../models/Dev');
const parseStringAsArray = require('../utils/parseStringAsArray');
const { findConnections, sendMessage } = require('../websocket');

module.exports = {
    async store (req, res){
        const { github_username, techs, latitude, longitude } = req.body;

        let dev = await Dev.findOne({ github_username });

        if(!dev){
            const apiResponse = await axios.get(`https://api.github.com/users/${github_username}`);
        
            const { name = login, avatar_url} = apiResponse.data;
        
            const techsArray = parseStringAsArray(techs);
        
            const location = {
                type: 'Point',
                coordinates: [longitude, latitude],
            }
        
            let bio = apiResponse.data.bio;
        
        
            if(bio == null){
                bio = "Sem biografia...";
            }
        
            dev = await Dev.create({
                github_username,
                name,
                bio,
                avatar_url,
                techs: techsArray,
                location,
            });

            const sendSocketMessageTo = findConnections(
                { latitude, longitude }, 
                techsArray,
            )

            console.log(sendSocketMessageTo);

            sendMessage(sendSocketMessageTo, 'new-dev', dev);

        }
        return res.json(dev);
    },
    async index(req, res){
        const devs = await Dev.find();

        return res.json(devs);
    },
    async destroy(req, res){

        const { github_username } = req.query;

        const remove = await Dev.deleteOne({github_username : github_username});

        if(remove.deletedCount > 0){
            return res.json({ "msg": `${github_username} foi deletado com sucesso!`});
        }else{
            return res.json({ "msg": `Não foi possível deletar ${github_username}`});
        }
    },
    async update(req, res){

        const { github_username, name, bio, latitude, longitude, techs } = req.query;
        
        let dev = await Dev.findOne({ github_username });

        if(dev != null){
            const location = {
                type: 'Point',
                coordinates: [longitude, latitude],
            }
            
            const techsArray = parseStringAsArray(techs);

            await Dev.findOneAndUpdate({
                github_username,
            }, {
                $set: {
                    name,
                    bio,
                    techs: techsArray,
                    location,
                }
            }, {
                new: true 
            }, (err, doc) => {
                if ( err ) {
                    console.error(err)
                    return res.json("Erro, não foi possível editar este usuário...");
                }
                return res.json("Usuário alterado com sucesso!");
            })
        }
    }
};